#include <Wire.h>
#include <Servo.h>
#include <NewPing.h>

#include <EEPROM.h>//dev

#define FORWARD 0
#define BACK 2
#define LEFT 1
#define RIGHT 3
#define STOP -1

#define NORTH 70

Servo mot1;
Servo mot2;
Servo Hand;

NewPing sonar(4, 5, 40);

int8_t pole[10][6] = //основной массив с полем
{
// |  -------> Y
// |
// ^ X
	{-1, -1, -1, -1, -1, -1, },
	{-1, 9, 11, 11, 3, -1, },
	{-1, 12, 6, 13, 7, -1, },
	{-1, 9, 10, 15, 7, -1, },
	{-1, 12, 3, 13, 6, -1, },
	{-1, 9, 15, 14, 3, -1, },
	{-1, 12, 15, 11, 6, -1, },
	{-1, 9, 7, 13, 3, -1, },
	{-1, 12, 14, 14, 6, -1, },
	{-1, -1, -1, -1, -1, -1, },
};

const int TimeTo30cm = 1700;//2200 ; 1900 - 8,2; Vss-1800

int8_t path[32][2];
int8_t PathForVolna[32];
int8_t NumOfSteps=0;

int8_t dx[4];
int8_t dy[4];

int8_t GlobalDerect;

int8_t numStatPoint = 2;

struct coord
{
	int8_t x;
	int8_t y;
	bool operator==(const coord& a) const
	{
		return (x == a.x && y == a.y);
	}
}

zero_cord,temp_cord, GlobalPosition, GlobalPositionStart, GlobalPositionDelta, end;

coord banka[3];

int8_t randomEachStep;

struct Sensors
{
	int8_t s1, s2, s3, s4, s5, s6, s7, s8, s9;// 0 BLACK ;1 wight

	//фонкция обновляет показания с датчикой
	void Update()//возможно проверка значений для точности
	{
		s1 = !digitalRead(A2);
		s2 = digitalRead(8);
		s3 = digitalRead(9);
		s4 = digitalRead(6);
		s5 = digitalRead(7);
		s6 = !digitalRead(A1);
		s7 = !digitalRead(A0);

		s8 = digitalRead(12);
		s9 = digitalRead(3);
	}
}sensors;

int8_t getCompas()
{
	Wire.beginTransmission(1); // talk to I2C device ID 1
	Wire.write(0x44); // direction register
	Wire.endTransmission(); // end transmission
	Wire.requestFrom(1, 2); // Request 2 bytes from ID 1
	while (Wire.available() < 2);
	uint16_t lb = Wire.read();
	uint16_t val = Wire.read();
	val <<= 8;
	val |= lb;

	val = (val + 45); // для удобства сравнения
	if (val < NORTH)
		val += 360;

	if ((val >= NORTH)&&(val <= NORTH + 90))
	{
		return 0;//FORWARD
	}
	if ((val > NORTH + 90)&&(val <= NORTH + 180))
	{
		return 3;//RIGHT;
	}
	if ((val > NORTH + 180)&&(val <= NORTH + 270))
	{
		return 2;//BACK;
	}
	if ((val > NORTH + 270)&&(val <= NORTH + 360))
	{
		return 1;//LEFT;
	}
}

// waitSensorValue
void waitSensorValue(int8_t & sens, int8_t znach = 1) {
	do
	{
		sensors.Update();
	} while (sens == znach);
}

struct Motor {

	unsigned long TimeToRotate = 1000;

	void Mot(int8_t dir)
	{
		switch (dir){
			case FORWARD :
			{
				M1(100);
				M2(100);
				break;
			}
			case STOP :
			{
				M1(0);
				M2(0);
				break;
			}
			case BACK :
			{
				M1(-100);
				M2(-100);
				break;
			}
		}
	}
	void BackForw()
	{
		Mot(BACK);

		do
		{
			sensors.Update();
		}
		while (sensors.s1 == 1 && sensors.s6 == 1);
			//todo
//проезжаем через черную линию на перекрестке
        do
		{
			sensors.Update();
		}
		while (sensors.s1 != 1 || sensors.s1 != 1);
/*
		waitSensorValue(sensors.s1, 1);
		waitSensorValue(sensors.s6, 1);

		waitSensorValue(sensors.s1, 0);
		waitSensorValue(sensors.s6, 0);
		*/
		Mot(STOP);
	}

	// TurnToLine
	void TurnToLine(int8_t dir)
	{
		Mot(STOP);
		switch (dir) {
			case FORWARD:
			{
				uint32_t time = millis();
				Mot(FORWARD);
				
				do
				{
					sensors.Update();
				} while (sensors.s2 && sensors.s5);
				
//проезжаем через черную линию на перекрестке
                do
				{
					sensors.Update();
				}
				while ((sensors.s2 != 1 || sensors.s5 != 1)&&(millis() - time < TimeTo30cm / 5));
				    // todo
/*
				waitSensorValue(sensors.s2, 0);
				waitSensorValue(sensors.s5, 0);//CHEK XXX
*/
				//может надо делай и стоп ?
				break;
			}
			case LEFT:
			{

				M2(100);
				delay(300);// FIXME не надёжно
                
                //waitSensorValue
				waitSensorValue(sensors.s7);
				//waitSensorValue(sensors.s4);//TODO

				Mot(STOP);


				/*
				Serial.print('case left');
				rightMotorPower(100);

				sensorsValue = updateSensors();

				if (sensorsValue.s4 == 1 || sensorsValue.s7 == 1) {
	               rightMotorPower(0).
	               rightMotor.stop();
				}

				*/

				break;
			}
			case RIGHT:
			{
				M1(100);
				delay(300); // FIXME не надёжно
				waitSensorValue(sensors.s7);
				//waitSensorValue(sensors.s3);

				Mot(STOP);
				break;
			}
			case BACK:
			{
				M1(-100);
				M2(100);

				delay(500);

				waitSensorValue(sensors.s1);

				waitSensorValue(sensors.s7);

				Mot(STOP);
				break;
			}
		}
		Mot(STOP);
	}
	
 
 // motorLeft
// der исправить на direction	
	void M1(int8_t dir)
	{
		mot1.write(map(dir, -100, 100, 0, 180));//не очень точно, map довольно жирный
	}
// motorRight
	void M2(int8_t dir)
	{
		mot2.write(map(dir, 100, -100, 0, 180));
	}
}motor;

/** не очень рекурсивная функция, основная задача которой сравнивать поле с путём (path),
	* возвращает 1 если при старте из i j путь валиден
	*/

int8_t findAround(int8_t i,int8_t j)// i, j = кординаты ; k = глубина
{
	for (int k = 0; k < NumOfSteps - 1; ++k)
	{
		if (pole[i + dx[ path[k][1] ]][j + dy[ path[k][1] ]] == path[k+1][0])
		{
			i += dx[ path[k][1] ];
			j += dy[ path[k][1] ];
		}
		else
		{
			return 0;
		}
	}
	GlobalPosition.x = i;
	GlobalPosition.y = j;
	return 1;
	/*
	if (path[k+1][0] == -1)
	{
		//save x,y
		GlobalPosition.x = i;
		GlobalPosition.y = j;
		return 1;
	}
	else if(pole[i + dx[ path[k][1] ]][j + dy[ path[k][1] ]] == path[k+1][0])
	{
		return findAround(i + dx[ path[k][1] ],j + dy[ path[k][1] ],k+1);
	}
	else
		return 0;
	*/
}

int8_t Find() {
	Serial.println("Find ");
	uint32_t startTimeForF = millis();

	int8_t num_p = 0; // переменная хранит количество возможных точек старта

	for (int8_t i = 1; i < 9; i++) {
		for (int8_t j = 1; j < 5; j++) {
			num_p += findAround(i,j);
		}
	}

	GlobalPositionStart = GlobalPosition;
	for (int8_t i = NumOfSteps-1; i > 0; i--) {
		//pole[i + dx[ path[k][1] ]][j + dy[ path[k][1] ]] == path[k+1][0]
		GlobalPositionStart.x -= dx[ path[i-1][1] ];
		GlobalPositionStart.y -= dy[ path[i-1][1] ];
	}
	temp_cord = GlobalPosition;

	if (num_p == 1)
		for (int8_t i = NumOfSteps-1; i > 0; i--) {
			if (path[i][0] / 16 % 2)
			{
				for (int b = 0; b < 3; ++b)
				{
					if (banka[b] == temp_cord)
					{
						zeroCord(banka[b]);
					}
				}
			}
			temp_cord.x -= dx[ path[i-1][1] ];
			temp_cord.y -= dy[ path[i-1][1] ];
		}
	Serial.print("Find end ");
	Serial.println(millis()-startTimeForF);
	return num_p;
}

void Move(unsigned long dell, int8_t der = FORWARD)
{
	Serial.println("in Move");
	unsigned long wait = millis();
	while(millis() - wait < dell)
	{
		sensors.Update();
		if(der == BACK)
		{
			motor.Mot(BACK);
			if (!sensors.s8)
			{
				motor.M1(0);
			}
			if (!sensors.s9)
			{
				motor.M2(0);
			}
			if ((!sensors.s9)&&(!sensors.s8))
			{
				motor.Mot(BACK);
			}
		}
		else
		{
			motor.Mot(FORWARD);
			if (!sensors.s3)
				motor.M1(0);
			if (!sensors.s4)
				motor.M2(0);
		}
		delay(10);
	}
	motor.Mot(STOP);
}

void handOpen()
{
	int pingDatch = sonar.ping_cm();
	motor.Mot(BACK);

	while(( pingDatch < 11)&&(pingDatch != 0)){
		pingDatch = sonar.ping_cm();
		delay(30);
	}

	motor.Mot(STOP);
/*	
	for (int pos = 0; pos <= 170; pos += 1) {//TODO ускорить
		Hand.write(pos);
		delay(10);
	}
*/
	Hand.write(180);
	delay(500);
}
void handClose()
{
	int DegrNunOfBank = 0;
	for (int i = 0; i < 3; ++i)
	{
		if (banka[i] == zero_cord)
		{
			DegrNunOfBank ++;
		}
	}
	Serial.print("handClose bank ");
	Serial.println(DegrNunOfBank);
	DegrNunOfBank *= 45;

	for (int pos = 180; pos >= DegrNunOfBank; pos -= 1) {//TODO ускорить
		Hand.write(pos);
		delay(10);
	}
	delay(100);
}

void turOnLine()//old : UpToLine
{
	motor.M1(-100);
	motor.M2(100);
	waitSensorValue(sensors.s7);
	motor.Mot(STOP);

	path[0][1] = getCompas();
	path[0][0] = 0;
	NumOfSteps = 1;

	GlobalDerect = path[0][1];
}



int8_t yTurnFromStep = 0;

int8_t helparr[10][6];//вспомогательный массив для волнового алгоритма, хранит номар шага

void Volna(coord start)//TODO поменять местами end и start чтоб не путаться;
{
	Serial.println("Volna start ");
	int8_t tmp;

	for (int8_t i = 0; i < 10; i++) {
		helparr[i][0] = -1;
		helparr[i][5] = -1;
	}
	for (int8_t i = 0; i < 6; i++) {
		helparr[0][i] = -1;
		helparr[9][i] = -1;
	}

	for (int8_t i = 0; i < 32; i++) {//а надо ли
		helparr[i % 8 + 1][i / 8 + 1] = 0;
	}

	int8_t flag(1);
	int8_t d(1);//номер шага
	helparr[start.x][start.y] = 1;
	while (flag) {
		flag = 0;
		for (int8_t i = 1; i <= 8; ++i) // x
		{
			for(int8_t j = 1; j <= 4; ++j ) // y
			{
				if (helparr[i][j] == d)//если следуюший шаг от сюда d?
				{
					tmp = pole[i][j];//в этой переменной "парситься" поле по степеням двойки
					for (int8_t m = 0; m < 4; ++m)
					{
						temp_cord.x = i + dx[m];
						temp_cord.y = j + dy[m];
						if ((tmp % 2)&&(helparr[i + dx[m]][j + dy[m]] == 0)
						//если можно повернуть и туда мы едем первый раз и
							&& !(banka[0] == temp_cord)&& !(banka[1] == temp_cord)&& !(banka[2] == temp_cord)) // работать будет но, неочевидно как
						// и там нет банки
						{
							helparr[i + dx[m]][j + dy[m]] = d + 1;
							flag = 1;//повторять пока есть изменения на поле
						}
						tmp /= 2;
					}
				}
			}
		}
		d++;
	}
	Serial.println("Volna end ");
	uint32_t startTimeForF = millis();
}

void MoveToNearest(coord start, coord end = GlobalPosition)
{

	Volna(start);

	Serial.print("MoveToNearest ");
	Serial.print(start.x);
	Serial.print(" ");
	Serial.println(start.y);

	int8_t tmp;
	int8_t x = end.x;
	int8_t y = end.y;

	//add else
	for (int8_t i = helparr[end.x][end.y] - 1; i >= 1 ; i--) {
		tmp = pole[x][y];//"парсинг" блока поля по степеням двойки
		for (int8_t m = 0; m < 4; ++m)
		{
			if ((tmp % 2)&&(helparr[x + dx[m]][y + dy[m]] == i))
			{
				Serial.println(pole[x][y]);
				Serial.println(m);
				Serial.println((m - GlobalDerect + 4)%4);

				//TODO вынести в фцнкцию

				if ((pole[x][y] == 5 || pole[x][y] == 10)
					||(yTurnFromStep == 3 /*compas*/))
				{
					
					if ((m - GlobalDerect + 4)%4 != 0)
					{
						motor.TurnToLine(BACK);

						if (yTurnFromStep == 3)
						{
							yTurnFromStep = step().y;
							Move(900);
						}

						GlobalDerect = getCompas();
				
					}
					
					Serial.print("WooooooooooW");
					Serial.println(yTurnFromStep);
				}
				else
				{
					motor.TurnToLine((m - GlobalDerect + 4)%4 );
				}

				GlobalDerect = getCompas();

				if (i == 1)
				{
					motor.Mot(STOP);
				}
				else
				{
					yTurnFromStep = step().y;
				}

				GlobalDerect = getCompas();
				
				x += dx[m];
				y += dy[m];

				GlobalPosition.x = x;
				GlobalPosition.y = y;

				/*
				cout << (int)m << " - ";
				cout << (int)dx[m] << " " << (int)dy[m] << " - ";//dev
				cout << (int)x << " " << (int)y << endl;//dev
				*/
				break;
			}
			tmp /= 2;
		}
	}
}

void compileBank()
{
	Serial.print("compileBank ");
	for (int8_t k = 0; k < 3; k++)
	{
		int8_t lenghtOfPath[3];

		Volna(GlobalPosition);

		for (int b = 0; b < 3; ++b)
		{
			if (!(banka[b] == zero_cord))
			{
				int8_t tmp = pole[banka[b].x][banka[b].y];//"парсинг" блока поля по степеням двойки
				int8_t min = 100;
				for (int8_t m = 0; m < 4; ++m)
				{
					if ((tmp % 2)&&(helparr[banka[b].x + dx[m]][banka[b].y + dy[m]] < min)&&(helparr[banka[b].x + dx[m]][banka[b].y + dy[m]] != 0))
					{
						min = helparr[banka[b].x + dx[m]][banka[b].y + dy[m]];
					}
					tmp /= 2;
				}
				lenghtOfPath[b] = min;
			}
			else
				lenghtOfPath[b] = 100;
		}

		int8_t chosenBanka;
		if (lenghtOfPath[0] > lenghtOfPath[1])
		{
			if (lenghtOfPath[1] > lenghtOfPath[2])
			{
				chosenBanka = 2;
			}
			else
			{
				chosenBanka = 1;
			}
		}
		else
		{
			if (lenghtOfPath[0] > lenghtOfPath[2])
			{
				chosenBanka = 2;
			}
			else
			{
				chosenBanka = 0;
			}
		}
		if (lenghtOfPath[chosenBanka] == 100)
		{
			return;
		}

		Serial.print("chosenBanka ");
		Serial.println(chosenBanka);
		//EEPROM.write(chosenBanka, banka[chosenBanka].x);

		MoveToNearest(banka[chosenBanka]);
		BankaEat();
		if ((pole[banka[chosenBanka].x][banka[chosenBanka].y] != 5+16) && (pole[banka[chosenBanka].x][banka[chosenBanka].y] != 10+16))
		{
			Move(TimeTo30cm / 30, BACK);
			step();
			motor.TurnToLine(FORWARD);
		}
		motor.Mot(STOP);
		zeroCord(banka[chosenBanka]);
	}
}


void setup()
{
	Serial.begin(9600);

	Wire.begin();// start I2C
	randomSeed(analogRead(0));

	mot1.attach(10);
	mot2.attach(11);
	Hand.attach(2);

	for (int8_t i = 6; i <= 9; ++i)
	{
		pinMode(i, INPUT);
	}

	pinMode(3, INPUT);
	pinMode(12, INPUT);

	zero_cord.x = 0;
	zero_cord.y = 0;

	dx[0]=1; dy[0]=0;
	dx[1]=0; dy[1]=-1;
	dx[2]=-1; dy[2]=0;
	dx[3]=0; dy[3]=1;

	GlobalPosition.x = -1;
	GlobalPosition.y = -1;
	GlobalPositionStart.x = -1;
	GlobalPositionStart.y = -1;

	GlobalPositionDelta = {0, 0};

	banka[0].x = 1;
	banka[0].y = 2;

	banka[1].x = 2;
	banka[1].y = 4;

	banka[2].x = 7;
	banka[2].y = 3;

	end = {4,2};

	for (int b = 0; b < 3; ++b)
	{
		pole[banka[b].x][banka[b].y] += 16;
	}

	pinMode(13, OUTPUT);

	digitalWrite(13, 0);

	motor.Mot(STOP);

	Hand.write(40);//close

	for (int8_t i = 0; i < 32; i++) {
		path[i][0] = -1;
		path[i][1] = 0;
	}
	turOnLine();
}

int8_t mypow2(int8_t exp)
{
	int8_t ret = 1;
	for (int i = 0; i < exp; ++i)
	{
		ret *= 2;
	}
	return ret;
}

//выбирает в какую сторону повернуть
int8_t ChooseDerection(int8_t derrect)
{
	int8_t tmp = 0;
	if ((derrect / mypow2(tmp) % 2 == 1 )&&(tmp != 2 )&&(tmp < 5))
		return tmp;

	if (randomEachStep)
	{
		tmp = 1;//LEFT
		if ((derrect / mypow2(tmp) % 2 == 1 )&&(tmp != 2 )&&(tmp < 5))
			return tmp;
		tmp = 3;//RIGHT
		if ((derrect / mypow2(tmp) % 2 == 1 )&&(tmp != 2 )&&(tmp < 5))
			return tmp;
	}
	else
	{
		tmp = 3;//RIGHT
		if ((derrect / mypow2(tmp) % 2 == 1 )&&(tmp != 2 )&&(tmp < 5))
			return tmp;
		tmp = 1;//LEFT
		if ((derrect / mypow2(tmp) % 2 == 1 )&&(tmp != 2 )&&(tmp < 5))
			return tmp;
	}
	return 2;
}

int8_t flagOfadd16 = 0;

void writePath(int8_t derrect,int8_t compas,int8_t NewDerect)
{
	path[NumOfSteps][0] = (derrect == 15 ? 15 : (derrect * mypow2(compas))%15 );// преобразует клетку поля
	path[NumOfSteps][0] += flagOfadd16 * 16;
	flagOfadd16 = 0;
	path[NumOfSteps][1] = (NewDerect + compas) % 4;//конструкция преобразует поворот из относительных роботу в глобальные кординаты
	NumOfSteps++;
}

//делает поворот, в на перекрёстке и
//  записывает это в путь (path)
// громоздко вынести в отдельную функцию
void TurnChoose(int8_t derrect)
{
	Serial.println("TurnChoose ");
	uint32_t startTimeForF = millis();

	int8_t NewDerect;
	int8_t compas = getCompas();//зачем? есть GlobalDerect

	if (derrect == 32)
	{
		derrect = checkField();
	}
	Serial.println(derrect);

	NewDerect = ChooseDerection(derrect);

	int dach = sonar.ping_median(3);

	if (compas == GlobalDerect)
	{
		if ((dach < 500)&&(dach != 0))//провверка на банку; для промого проезда, замерить REVIEW
		{
			//записать банку

			Serial.println("Eat in Turn");

			BankaEat();
			
			flagOfadd16 = 1;

			writePath(derrect, compas, NewDerect);
		}
		else
		{
			writePath(derrect, compas, NewDerect);
		}

		if (NumOfSteps > 1)
			numStatPoint = Find();

		if ((derrect != 5)&&(NewDerect != 2)&&(numStatPoint > 1))
		{
			motor.TurnToLine(NewDerect);
		}
		GlobalDerect = getCompas();
	}
	else
	{
		Serial.println("compas != GlobalDerect");
		if ((dach < 500)&&(dach != 0))//не должно быть так
		{

			/*
			writePath(derrect, GlobalDerect, NewDerect);

			BankaEat();

			flagOfBankaYes = 1;
			*/
			Blink(100);
			GlobalDerect = getCompas();
		}
		else
		{
			writePath(derrect, GlobalDerect, NewDerect);
			GlobalDerect = getCompas();

		}
		if (NumOfSteps > 1)
			numStatPoint = Find();
	}
	Serial.print("TurnChoose end ");
	Serial.println(millis()-startTimeForF);
}

void BankaEat()
{
	Serial.println("in BankaEat");
	handOpen();

	int8_t moving = true;

	int8_t s1storage = 0, s6storage = 0;
/*
	motor.M1(10);
	waitSensorValue(sensors.s2);
	motor.Mot(STOP);
*/
	unsigned long startTime = millis();

	yTurnFromStep = 0;

	while(moving) {
		sensors.Update();
		motor.M1(10);
		motor.M2(10);

		if (!sensors.s3)
			motor.M1(0);
		if (!sensors.s4)
			motor.M2(0);

/*
		if (sensors.s3)
			motor.M1(0);
		if (sensors.s2)
			motor.M2(0);
*/
		
		//add delay
		delay(1);//old 20

		sensors.Update();

		if (!sensors.s1)
			s1storage++;
		if (!sensors.s6)
			s6storage++;

		int dach = sonar.ping();

		if (millis() - startTime >= TimeTo30cm / 3 * 2 )//1 millis TODO /2
		{
			Serial.println("millis");
			moving = false;
			motor.Mot(STOP);
		}
		else  if ((sensors.s1 > 5) || (s6storage > 5))//2 field
		{
			Serial.println("field");
			moving = false;
			motor.Mot(STOP);
		}
		else if ((dach < 350)&&(dach != 0))//TODO 260
		{
			moving = false;
			motor.Mot(STOP);
		}

	}

	motor.M1(10);
	waitSensorValue(sensors.s2);
	motor.Mot(STOP);

	handClose();
	
	motor.M1(-10);
	waitSensorValue(sensors.s3, 0);
	motor.Mot(STOP);

/*
	motor.M2(-10);
	motor.Mot(BACK);
	waitSensorValue(sensors.s2, 0);
	Move(200);
*/
	motor.Mot(STOP);
}

void Blink(int dell)
{
	digitalWrite(13, 1);
	delay(dell);
	digitalWrite(13, 0);
	delay(100);
}

int8_t checkField()
{
	Serial.println("in checkField");
	sensors.Update();
	motor.Mot(STOP);

	if (!sensors.s6)
		while(sensors.s5 && sensors.s1)//тут поворачиваемся, для того чтобы исключить ложно-положительный результат: сомнительная функция
		{
			motor.M1(100);//TODO = 10
			//motor.M2(5);
			sensors.Update();
		}
	motor.Mot(STOP);
	if (!sensors.s1)
		while(sensors.s2 && sensors.s6)
		{
			motor.M2(100);
			//motor.M1(5);
			sensors.Update();
		}

	motor.Mot(STOP);//TODO in vecher
	motor.M1(10);
	waitSensorValue(sensors.s8, 0);
	
	motor.Mot(STOP);
	motor.M2(10);
	waitSensorValue(sensors.s9, 0);


	motor.Mot(STOP);

	//dev TODO убрать
	//delay(2000);

	delay(50);
	sensors.Update();

	Serial.println("checkField end ");
	// XXX подумать над более понимаемым обозначением
	if ((sensors.s7)&&(sensors.s6 != sensors.s1))//2 
	{
		return ((!sensors.s1 ? (2+4) : (8+4)));//2+4 это поворот на лево ; 8+4 поворот на прво (относительно робота) с.м.README
	}
	if ((!sensors.s7)&&(sensors.s6 != sensors.s1))//3 |-
	{
		return ((!sensors.s1 ? (2+4+1) : (8+4+1)));
	}
	if ((sensors.s7)&&(sensors.s6 == 0)&&(sensors.s1 == 0))//3 T
	{
		return (4+2+8);
	}
	if ((!sensors.s7)&&(!sensors.s6)&&(!sensors.s1))//4
	{
		return (15);
	}

	//motor.TurnToLine(FORWARD);
}

void zeroCord(coord & bank)
{
	pole[bank.x][bank.y] -= 16;
	bank = zero_cord;
}


void getOut()//уезд от банок
{
	Serial.println("in getOut");
	uint32_t startTimeForF = millis();
	int8_t moving = true;
	unsigned long startTime = millis();

	while(moving) {
		sensors.Update();

		motor.Mot(BACK);
		if (!sensors.s8)
		{
			motor.M1(0);
		}
		if (!sensors.s9)
		{
			motor.M2(0);
		}
		if ((!sensors.s9)&&(!sensors.s8))
		{
			motor.Mot(BACK);
		}
		delay(5);

		if (millis() - startTime >= TimeTo30cm )//1 millis
		{
			Serial.println("millis");
			moving = false;
		}
		else  if (!sensors.s1 || !sensors.s6)//2 field
		{
			Serial.println("field");
			moving = false;			
		}
		else if (getCompas() != GlobalDerect)//3 compas
		{
			Serial.println("compas");
			Move(900, BACK);
			moving = false;		
		}
	}

	motor.Mot(STOP);
	GlobalPosition.x += dx[(GlobalDerect + 2)%4];
	GlobalPosition.y += dy[(GlobalDerect + 2)%4];

	GlobalDerect = getCompas();

	Serial.println("getOut end");
	Serial.println(millis()-startTimeForF);
}

coord step()
{
	Serial.println("in step");
	uint32_t startTimeForF = millis();

	randomEachStep = random(2);//0..1

	int8_t moving = true;
	unsigned long startTime = millis();

	int8_t s1storage = 0, s6storage = 0;

	while(moving) {
		sensors.Update();
		motor.Mot(FORWARD);
		if (!sensors.s3)
			motor.M1(0);
		if (!sensors.s4)
			motor.M2(0);

		
		//add delay
		delay(1);//old 20

		sensors.Update();

		if (!sensors.s1)
			s1storage++;
		if (!sensors.s6)
			s6storage++;

		int dach = sonar.ping();

		if (millis() - startTime >= TimeTo30cm )//1 millis
		{
			Serial.println("millis");
			moving = false;
			motor.Mot(STOP);
			Serial.println("step end");
			Serial.println(millis()-startTimeForF);

			return {5, 1};
		}
		else  if ((s1storage > 2) || (s6storage > 2))//2 field
		{
			Serial.println("field");
			moving = false;
			motor.Mot(STOP);
			Serial.println("step end");
			Serial.println(millis()-startTimeForF);

			return {32, 2};
		}
		else if (getCompas() != GlobalDerect)//3 compas
		{
			Serial.println("compas");
			moving = false;

			Move(800); //move TODO
Blink(100);
			motor.Mot(STOP);
			Serial.println("step end");
			Serial.println(millis()-startTimeForF);

			return {(( ((GlobalDerect - getCompas()) ==  1)||((GlobalDerect - getCompas()) ==  -3)) ? (4+8) : (4+2)), 3};
		}
	}
	//motor.Mot(STOP);
}
void RotorBefoOutAndHandOpen()
{
	Volna(GlobalPositionStart);

	int8_t tmp_pole = pole[GlobalPosition.x][GlobalPosition.y];//"парсинг" блока поля по степеням двойки
	int8_t min = 100;
	int8_t min_m[4] = {0, 0, 0, 0};
	for (int8_t m = 0; m < 4; ++m)
	{
		if ((tmp_pole % 2)&&(helparr[GlobalPosition.x + dx[m]][GlobalPosition.y + dy[m]] < min)&&(helparr[GlobalPosition.x + dx[m]][GlobalPosition.y + dy[m]] != 0))
		{
			min = helparr[GlobalPosition.x + dx[m]][GlobalPosition.y + dy[m]];

		}
		min_m[m] = helparr[GlobalPosition.x + dx[m]][GlobalPosition.y + dy[m]];
		Serial.print("min_m[m] ");
		Serial.print(m);
		Serial.print(" ");
		Serial.println(min_m[m]);
		tmp_pole /= 2;
	}
	if (min_m[2] != 0)
	{
		handOpen();
	}
	else if (min_m[0] != 0)
	{
		motor.TurnToLine(BACK);
		handOpen();
	}
	else if (min_m[1] != 0)
	{
		motor.M1(100);
		waitSensorValue(sensors.s9);
		motor.M2(-100);
		waitSensorValue(sensors.s8);
		waitSensorValue(sensors.s9);
		motor.Mot(STOP);
		handOpen();
	}
	else if (min_m[3] != 0)
	{
		motor.M2(100);
		waitSensorValue(sensors.s8);
		motor.M1(-100);
		waitSensorValue(sensors.s9);
		waitSensorValue(sensors.s8);
		motor.Mot(STOP);
		handOpen();
	}

	GlobalDerect = getCompas();
}

void mainProgram()
{

	/*
	*/

	coord tmp;

	//TurnChoose(step().x);
	do
	{
		tmp = step();
		TurnChoose(tmp.x);
		motor.Mot(STOP);
		
		//numStatPoint--;//dev
		Serial.print(" -------- ");
		Serial.println(numStatPoint);
	}while(numStatPoint > 1);

	yTurnFromStep = tmp.y;

	for (int i = 0; i < 5; ++i)//dev
	{
		EEPROM.write(i, path[i][0]);
		Serial.print(path[i][0]);
		Serial.print(" - ");
		Serial.println(path[i][1]);
	}

	if (numStatPoint == 0) //numStartPoint
	{
		motor.Mot(STOP);
		Blink(800);

		for (int8_t i = NumOfSteps-1; i > 0; i--) {
			//pole[i + dx[ path[k][1] ]][j + dy[ path[k][1] ]] == path[k+1][0]
			GlobalPositionDelta.x -= dx[ path[i-1][1] ];
			GlobalPositionDelta.y -= dy[ path[i-1][1] ];
		}

		for (int8_t i = 0; i < 32; i++) {
			path[i][0] = -1;
			path[i][1] = 0;
		}
		turOnLine();
		return;
	}

	GlobalPositionStart.x += GlobalPositionDelta.x;
	GlobalPositionStart.y += GlobalPositionDelta.y;

	if (numStatPoint == 1)
	{
		// Be happy
		//Blink(200);
		//EEPROM.write(6, GlobalPositionStart.x);
		//EEPROM.write(7, GlobalPositionStart.y);
		Serial.print(GlobalPositionStart.x);
		Serial.print(" - ");
		Serial.print(GlobalPositionStart.y);
	}
	
	delay(500);//dev

	compileBank();

	MoveToNearest(end);

	banka[0] = end;


	if ((pole[end.x][end.y] == 5)||(pole[end.x][end.y] == 10))
	{
		Move(TimeTo30cm * 9 / 12 );//TODO 9/12
		motor.Mot(STOP);
		RotorBefoOutAndHandOpen();
		getOut();
	}
	else
	{
		step();
		motor.TurnToLine(FORWARD);
		Move(TimeTo30cm / 15);
		RotorBefoOutAndHandOpen();
		motor.BackForw();
		Move(TimeTo30cm / 10, BACK);
		getOut();
	}

//dop
	if (GlobalPositionStart.x > 4)
		GlobalPositionStart.x = 8;
	else
		GlobalPositionStart.x = 1;

	if (GlobalPositionStart.y > 2)
		GlobalPositionStart.y = 1;
	else
		GlobalPositionStart.y = 4;

	MoveToNearest(GlobalPositionStart);
	step();
	delay(15000);
}

void loop()
{
	mainProgram();
	//Move(TimeTo30cm);
	//delay(6000);
	/*
	sensors.Update();
	Serial.print(sensors.s1);
	Serial.print("  ");
	Serial.print(sensors.s2);
	Serial.print("  ");
	Serial.print(sensors.s3);
	Serial.print("  ");
	Serial.print(sensors.s4);
	Serial.print("  ");
	Serial.print(sensors.s5);
	Serial.print("  ");
	Serial.print(sensors.s6);
	Serial.print("  ");
	Serial.print(sensors.s7);
	Serial.print("  ");
	Serial.print(sensors.s8);
	Serial.print("  ");
	Serial.print(sensors.s9);
	Serial.println("  ");
	*/
}