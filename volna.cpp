#include <iostream>

using namespace std;

struct coord
{
	int8_t x;
	int8_t y;
	bool operator==(const coord& a) const
	{
	    return (x == a.x && y == a.y);
	}
}temp_cord, GlobalPosition, GlobalPositionStart;

coord banka[3];

int main(int argc, char const *argv[])
{
	//globall
	int8_t pole[10][6] = //REVIEW == a , нужно ли , можно использовать А , но сложнее
{
		{-1, -1, -1, -1, -1, -1, },
	{-1, 9, 11, 11, 3, -1, },
	{-1, 12, 6, 13, 7, -1, },
	{-1, 9, 5, 15, 7, -1, },
	{-1, 12, 3, 13, 6, -1, },
	{-1, 9, 15, 14, 3, -1, },
	{-1, 12, 15, 11, 6, -1, },
	{-1, 9, 7, 13, 3, -1, },
	{-1, 12, 14, 14, 6, -1, },
	{-1, -1, -1, -1, -1, -1, },
};
	int8_t tmp;
	int8_t b[10][6];//вспомогательный массив для волнового алгоритма, хранит номар шага

	int8_t dx[4];
	int8_t dy[4];

	dx[0]=1; dy[0]=0;
	dx[1]=0; dy[1]=-1;
	dx[2]=-1; dy[2]=0;
	dx[3]=0; dy[3]=1;
	//end global
	//рамки, чтобы не выйти за границы
	for (int8_t i = 0; i < 10; i++) {
		b[i][0] = -1;
		b[i][5] = -1;
	}
	for (int8_t i = 0; i < 6; i++) {
		b[0][i] = -1;
		b[9][i] = -1;
	}

	for (int8_t i = 0; i < 32; i++) {
		b[i % 8 +1][i / 8 +1] = 0;
	}

	//dev begin вывод
	for (int8_t j = 1; j <= 4; j++) {
		for (int8_t i = 1; i <= 8; i++) {
			cout << (int)pole[i][j]<< " " ;
		}
		cout << endl;
	}
	cout << endl;
	//dev end
	coord start; //get from f() start.x(y) получаем из аргументов к функции
	start.x = 4;//dev
	start.y = 3;
	banka[0].x = 3;
	banka[0].y = 2;

	banka[1].x = 0;
	banka[1].y = 0;

	banka[2].x = 0;
	banka[2].y = 0;


	int8_t flag(1);
	int8_t d(1);//номер шага
	b[start.x][start.y] = 1;
	while (flag) {
		flag = 0;
		for (int8_t i = 1; i <= 8; ++i) // x
		{
			for(int8_t j = 1; j <= 4; ++j ) // y
			{
				if (b[i][j] == d)//если следуюший шаг от сюда
				{
					tmp = pole[i][j];//в этой переменной "парситься" поле по степеням двойки
					for (int8_t m = 0; m < 4; ++m)
					{
						temp_cord.x = i + dx[m];
						temp_cord.y = j + dy[m];
						if ((tmp % 2)&&(b[i + dx[m]][j + dy[m]] == 0)
						//если можно повернуть и туда мы едем первый раз и
							&& !(banka[0] == temp_cord)&& !(banka[1] == temp_cord)&& !(banka[2] == temp_cord)) // работать будет но, неочевидно как
						// и там нет банки
						{
							b[i + dx[m]][j + dy[m]] = d + 1;
							flag = 1;//повторять пока есть изменения на поле
						}
						tmp /= 2;
					}
				}
			}
		}
		d++;
	}
	//dev begin вывод
	for (int8_t j = 1; j <= 4; j++) {
		for (int8_t i = 1; i <= 8; i++) {
			cout << (int)b[i][j]<< " " ;
		}
		cout << endl;
	}
	//dev end
	coord end;//получаем из аргументов к функции
	end.x = 3;
	end.y = 2;
	// IDEA поменять местами end & start , не надо буде реверсировать путь
	//востанавливаем путь
	int8_t x = end.x;
	int8_t y = end.y;
	for (int8_t i = b[end.x][end.y]-1; i >= 1 ; i--) {
		tmp = pole[x][y];//"парсинг" блока поля по степеням двойки
		for (int8_t m = 0; m < 4; ++m)
		{
			if ((tmp % 2)&&(b[x + dx[m]][y + dy[m]] == i))
			{
				//TODO запомнинаем путь
				if (i == 1)
				{
					cout  << " -----------------+++ - "<< endl;
				}
				cout << (int)m << " - ";
				cout << (int)dx[m] << " " << (int)dy[m] << " - ";//dev
				cout << (int)x << " " << (int)y << endl;//dev
				x += dx[m];
				y += dy[m];
				break;
			}
			tmp /= 2;
		}
	}
	return 0;
}
